/**
 * Created by Jacob Stuart on 7/23/14.
 */
import java.util.Stack;
public class RPN {
	private String equation;

	public RPN(String equation) {
		this.equation = equation;
	}

	public String getEquation() {

		return equation;
	}

	public int solve(){
		char[] eq = equation.toCharArray();
		Stack<Integer> st = new Stack<Integer>();
		for (char ch : eq){
			System.out.println(st.toString());
			if(Character.isDigit(ch)){
				st.push(ch - '0');

			} else{
				System.out.println(ch);
				switch (ch){
					case('+'):
						st.push(st.pop() + st.pop());
						break;
					case('-'):
						int x = st.pop();
						int y = st.pop();
						System.out.println("Subbing");
						System.out.printf("%d - %d\n", x, y);
						st.push(y - x);
						break;
					case('x'):
					case('*'):
						st.push(st.pop() * st.pop());
						break;
					case('/'):
						st.push(st.pop() / st.pop());
						break;
				}
			}
		}
		return st.pop();
	}


}
